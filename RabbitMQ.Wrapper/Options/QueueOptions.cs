﻿namespace RabbitMQ.Wrapper.Options
{
    public class QueueOptions
    {
        public string Name { get; set; } = string.Empty;

        public string Key { get; set; } = string.Empty;
    }
}
