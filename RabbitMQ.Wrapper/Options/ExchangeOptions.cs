﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Options
{
    public class ExchangeOptions
    {
        public const string Key = "Exchange";

        public string Name { get; set; } = string.Empty;

        public string Type { get; set; } = ExchangeType.Direct;
    }
}
