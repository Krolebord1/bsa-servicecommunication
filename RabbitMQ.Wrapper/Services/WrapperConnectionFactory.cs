﻿using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Options;

namespace RabbitMQ.Wrapper
{
    public class WrapperConnectionFactory : IConnectionFactory
    {
        private readonly ConnectionFactory _factory;

        public WrapperConnectionFactory(IOptions<RabbitMqOptions> rabbitMqOptions)
        {
            var options = rabbitMqOptions.Value;

            _factory = new ConnectionFactory
            {
                HostName = options.HostName,
                Port = options.Port,
                UserName = options.Username,
                Password = options.Password
            };
        }

        public IConnection CreateConnection()
        {
            return _factory.CreateConnection();
        }
    }
}
