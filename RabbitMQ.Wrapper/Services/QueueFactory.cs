﻿using RabbitMQ.Wrapper.Options;

namespace RabbitMQ.Wrapper
{
    public class QueueFactory : IQueueFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public QueueFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IQueue CreateQueue(ExchangeOptions exchange, QueueOptions queueOptions)
        {
            return new Queue(exchange, queueOptions, _connectionFactory);
        }
    }
}
