﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Options;

namespace RabbitMQ.Wrapper
{
    public class Queue : IQueue
    {
        private readonly ExchangeOptions _exchange;
        private readonly QueueOptions _queue;

        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly EventingBasicConsumer _consumer;

        private bool _consuming;
        private string _consumeTag = string.Empty;
        private readonly List<EventHandler<BasicDeliverEventArgs>> _onReceivedCallbacks = new();

        public Queue(ExchangeOptions exchangeOptions, QueueOptions queueOptions, IConnectionFactory connectionFactory)
        {
            _exchange = exchangeOptions;
            _queue = queueOptions;

            _connection = connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();

            DeclareExchange();
            DeclareAndBindQueue();

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += OnReceived;
        }

        public void ListenQueue(EventHandler<BasicDeliverEventArgs> onReceived)
        {
            _onReceivedCallbacks.Add(onReceived);

            StartConsume();
        }

        public void StopListenQueue()
        {
            _onReceivedCallbacks.Clear();

            StopConsume();
        }

        private void OnReceived(object? sender, BasicDeliverEventArgs args)
        {
            _channel.BasicAck(args.DeliveryTag, false);

            foreach (var onReceivedCallback in _onReceivedCallbacks)
                onReceivedCallback.Invoke(sender, args);
        }

        public void SendMessageToQueue(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(_exchange.Name, _queue.Key, null, body);
        }

        private void DeclareExchange()
        {
            _channel.ExchangeDeclare(_exchange.Name, _exchange.Type);
        }

        private void DeclareAndBindQueue()
        {
            _channel.QueueDeclare(_queue.Name, false, false, false);
            _channel.QueueBind(_queue.Name, _exchange.Name, _queue.Key);
        }

        private void StartConsume()
        {
            if(_consuming)
                return;

            _consumeTag = _channel.BasicConsume(_queue.Name, false, _consumer);
            _consuming = true;
        }

        private void StopConsume()
        {
            if(!_consuming)
                return;

            _channel.BasicCancel(_consumeTag);
            _consuming = false;
        }

        public void Dispose()
        {
            _connection.Dispose();
            _channel.Dispose();
        }
    }
}
