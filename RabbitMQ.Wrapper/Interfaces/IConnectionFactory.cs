﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    public interface IConnectionFactory
    {
        public IConnection CreateConnection();
    }
}
