﻿using RabbitMQ.Wrapper.Options;

namespace RabbitMQ.Wrapper
{
    public interface IQueueFactory
    {
        public IQueue CreateQueue(ExchangeOptions exchange, QueueOptions queueOptions);
    }
}
