﻿using System;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper
{
    public interface IQueue : IDisposable
    {
        public void ListenQueue(EventHandler<BasicDeliverEventArgs> onReceived);

        public void StopListenQueue();

        public void SendMessageToQueue(string message);
    }
}
