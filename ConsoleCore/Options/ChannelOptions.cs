﻿using RabbitMQ.Wrapper.Options;

namespace ConsoleCore
{
    public class ChannelOptions
    {
        public const string Key = "Channel";

        public ExchangeOptions Exchange { get; set; } = default!;

        public QueueOptions PingQueue { get; set; } = default!;

        public QueueOptions PongQueue { get; set; } = default!;
    }
}
