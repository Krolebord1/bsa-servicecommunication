﻿namespace ConsoleCore
{
    public interface IDIContainer
    {
        public T GetService<T>();
    }
}
