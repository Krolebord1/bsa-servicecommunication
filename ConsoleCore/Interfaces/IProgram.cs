﻿namespace ConsoleCore
{
    public interface IProgram
    {
        public void Run();
    }
}
