﻿using System;

namespace ConsoleCore
{
    public abstract class ProgramBase : IProgram
    {
        public abstract void Run();

        protected void WriteLineWithTimeStamp(string message)
        {
            Console.Write(DateTime.Now.ToLongTimeString().PadLeft(12).PadRight(14));
            Console.WriteLine(message);
        }
    }
}
