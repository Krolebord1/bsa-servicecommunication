﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Options;

namespace ConsoleCore
{
    public class DefaultContainer : IDIContainer
    {
        private readonly IServiceProvider _provider;

        public DefaultContainer(Action<IServiceCollection> configureServices)
        {
            var services = new ServiceCollection();

            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("properties.json", optional: false)
                .Build();

            services.AddSingleton(configuration);

            services.AddOptions<RabbitMqOptions>().BindConfiguration(RabbitMqOptions.Key);

            services.AddOptions<ChannelOptions>().BindConfiguration(ChannelOptions.Key);

            services.AddSingleton<IConnectionFactory, WrapperConnectionFactory>();

            services.AddSingleton<IQueueFactory, QueueFactory>();

            configureServices(services);

            _provider = services.BuildServiceProvider();
        }

        public T GetService<T>() =>
            _provider.GetService<T>() ?? throw new DependencyInjectionException(typeof(T));
    }

    public class DependencyInjectionException : Exception
    {
        public DependencyInjectionException(Type type)
            : base($"Type <{type.FullName}> is not registered inside DI container") {}
    }
}
