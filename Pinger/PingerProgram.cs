﻿using System;
using System.Text;
using System.Threading;
using ConsoleCore;
using Microsoft.Extensions.Options;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;

namespace Pinger
{
    public class PingerProgram : ProgramBase, IDisposable
    {
        private readonly TimeSpan _sendDelay = TimeSpan.FromMilliseconds(2500);

        private readonly IQueue _pingQueue;

        private readonly IQueue _pongQueue;

        public PingerProgram(IOptions<ChannelOptions> channelOptions, IQueueFactory queueFactory)
        {
            var options = channelOptions.Value;

            _pingQueue = queueFactory.CreateQueue(options.Exchange, options.PingQueue);

            _pongQueue = queueFactory.CreateQueue(options.Exchange, options.PongQueue);
        }

        public override void Run()
        {
            WriteLineWithTimeStamp("Pinger started");

            Console.WriteLine("Press [enter] to exit");
            Console.WriteLine();

            SendPing();

            _pongQueue.ListenQueue(OnPongReceived);

            Console.ReadLine();
        }

        private void OnPongReceived(object? sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body.ToArray());

            WriteLineWithTimeStamp($"\"{message}\" received");

            _pongQueue.StopListenQueue();

            Thread.Sleep(_sendDelay);

            SendPing();

            _pongQueue.ListenQueue(OnPongReceived);
        }

        private void SendPing()
        {
            _pingQueue.SendMessageToQueue("Ping");
            WriteLineWithTimeStamp("\"Ping\" sent");
        }

        public void Dispose()
        {
            _pingQueue.Dispose();
            _pongQueue.Dispose();
        }
    }
}
