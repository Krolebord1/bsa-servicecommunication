﻿using System;
using System.Text;
using System.Threading;
using ConsoleCore;
using Microsoft.Extensions.Options;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;

namespace Ponger
{
    public class PongerProgram : ProgramBase, IDisposable
    {
        private readonly TimeSpan _sendDelay = TimeSpan.FromMilliseconds(2500);

        private readonly IQueue _pingQueue;

        private readonly IQueue _pongQueue;

        public PongerProgram(IOptions<ChannelOptions> channelOptions, IQueueFactory queueFactory)
        {
            var options = channelOptions.Value;

            _pingQueue = queueFactory.CreateQueue(options.Exchange, options.PingQueue);

            _pongQueue = queueFactory.CreateQueue(options.Exchange, options.PongQueue);
        }

        public override void Run()
        {
            WriteLineWithTimeStamp("Ponger started");

            Console.WriteLine("Press [enter] to exit");
            Console.WriteLine();

            _pingQueue.ListenQueue(OnPingReceived);

            Console.ReadLine();
        }

        private void OnPingReceived(object? sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body.ToArray());

            WriteLineWithTimeStamp($"\"{message}\" received");

            _pingQueue.StopListenQueue();

            Thread.Sleep(_sendDelay);

            SendPong();

            _pingQueue.ListenQueue(OnPingReceived);
        }

        private void SendPong()
        {
            _pongQueue.SendMessageToQueue("Pong");
            WriteLineWithTimeStamp("\"Pong\" sent");
        }


        public void Dispose()
        {
            _pingQueue.Dispose();
            _pongQueue.Dispose();
        }
    }
}
