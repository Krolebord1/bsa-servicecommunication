﻿using System;
using ConsoleCore;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Wrapper.Options;

namespace Ponger
{
    class Program
    {
        public const string AppTitle = "Ponger | BSA 2021 Task 2";

        public static IDIContainer DI { get; private set; } = default!;

        static void Main()
        {
            Console.Title = AppTitle;

            DI = new DefaultContainer(ConfigureServices);

            var program = DI.GetService<IProgram>();
            program.Run();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions<QueueOptions>().BindConfiguration("PingQueue");

            services.AddSingleton<IProgram, PongerProgram>();
        }
    }
}
